<html>
<head>
  <link rel="stylesheet" href="http://auckland.bauer.uh.edu/Students/gl1536/TP2/styles.css">
</head>
<body bgcolor="#cccccc" style="font-family: Arial">
	<table border="1">
		<tr><td valign='middle' bgcolor='#999999'><a href="http://auckland.bauer.uh.edu/Students/gl1536/TP2"><img src="captsm.gif"></a></td>
		<td valign='middle' bgcolor='#999999'>
		<center><font color="#ffffff">
		<br>
		<b> MODIFY an Existing Account <br> Description in glmaster <br> (a42.asp) </b> <br>
		</td></tr>
	</table>
	</center>

	<%
	'
	'*** globals
	'
	dim cn
	dim rs
	dim tokenvalue
	sub pass1
	%>
	<p>
	<form name="f1" action="http://auckland.bauer.uh.edu/Students/gl1536/TP2/asp/a42.asp" METHOD="POST">
		<b>Account to Modify:</b>
		<table border="1">
			<tr>
				<td>MAJOR</td><td>MINOR</td><td>SUB 1</td><td>SUB 2</td>
			</tr>
			<tr>
				<td>
				<INPUT TYPE ="TEXT" NAME="major" size="4" maxlength="4"></td><td>
				<INPUT TYPE ="TEXT" NAME="minor" size="4" maxlength="4"></td><td>
				<INPUT TYPE ="TEXT" NAME="sub1" size="4" maxlength="4"></td><td>
				<INPUT TYPE ="TEXT" NAME="sub2" size="4" maxlength="4"></td>
			</tr>
			<tr><center>
				<td colspan="4" align="center"><b><INPUT TYPE="SUBMIT" NAME="sbutton" VALUE="Proceed to Modify"></b></td>
			</center></tr>
		</table>
		<input type="hidden" name="token" value="2"><p>
	</form>

	<%
	end sub

	sub pass2
	  response.write "<i><P>Pass 2 tokenvalue="+cstr(tokenvalue)
	  set rs=Server.CreateObject("ADODB.Recordset")

	  SQLString= "SELECT * FROM glmaster WHERE major="+request.form("major")
	  SQLString= SQLString + " AND minor="+request.form("minor")
	  SQLString= SQLString + " AND sub1="+request.form("sub1")
	  SQLString= SQLString + " AND sub2="+request.form("sub2")+";"

	  response.write "<p>Open glmaster rs with SQL--->"+cstr(SQLString)+"<------ "
	  rs.open SQLString,"DSN=gl1536;UID=gl1536;PWD=NFP32xcsX;"
	  response.write "Recordset opened OK"

	   c=0
	   while NOT rs.EOF
	      c=c+1
	      majfound=rs("major")
	      minfound=rs("minor")
	      s1found=rs("sub1")
	      s2found=rs("sub2")
	      desfound=rs("acctdesc")
	      balfound=rs("balance")
	      rs.movenext
	   wend
	   rs.close
	   set rs=nothing

	   if c=0 then
	      response.write "<P>Record not found for Account Number: <br>"
	      response.write "Major:"+request.form("major")+" Minor:"+request.form("minor")+" SUB1:"+request.form("sub1")+" SUB2:"+request.form("sub2")+"</i>"
	%>

	<p>
	<form name="f1.2" action="http://auckland.bauer.uh.edu/Students/gl1536/TP2/asp/a42.asp" METHOD="POST">
		<b>Account to Modify:</b>
		<table border="1">
			<tr>
				<td>MAJOR</td><td>MINOR</td><td>SUB 1</td><td>SUB 2</td>
			</tr>
			<tr>
				<td>
				<INPUT TYPE ="TEXT" NAME="major" size="4" maxlength="4"></td><td>
				<INPUT TYPE ="TEXT" NAME="minor" size="4" maxlength="4"></td><td>
				<INPUT TYPE ="TEXT" NAME="sub1" size="4" maxlength="4"></td><td>
				<INPUT TYPE ="TEXT" NAME="sub2" size="4" maxlength="4"></td>
			</tr>
			<tr><center>
				<td colspan="4" align="center"><b><INPUT TYPE="SUBMIT" NAME="sbutton" VALUE="Proceed to Modify"></b></td>
			</center></tr>
		</table>
		<input type="hidden" name="token" value="2"><p>
	</form>
	 
	<%
	    else
	%>

	<form name="f2" action="http://auckland.bauer.uh.edu/Students/gl1536/TP2/asp/a42.asp" METHOD="POST">
		<p></i><b>Modify the Account Description below then click 'Modify':</b>
		<table border="1">
			<tr>
				<td>MAJOR</td><td>MINOR</td><td>SUB 1</td><td>SUB 2</td><td>ACCOUNT DESCRIPTION</td><td>BALANCE</td>
			</tr>
			<tr>
				<td><INPUT TYPE ="hidden" NAME="major" size="4" maxlength="4" value="<%=cstr(majfound)%>"><%=cstr(majfound)%></td>
				<td><INPUT TYPE ="hidden" NAME="minor" size="4" maxlength="4" value="<%=cstr(minfound)%>"><%=cstr(minfound)%></td>
				<td><INPUT TYPE ="hidden" NAME="sub1" size="4" maxlength="4" value="<%=cstr(s1found)%>"><%=cstr(s1found)%></td>
				<td><INPUT TYPE ="hidden" NAME="sub2" size="4" maxlength="4" value="<%=cstr(s2found)%>"><%=cstr(s2found)%></td>
				<td><INPUT TYPE ="TEXT" NAME="acctdesc" size="40" maxlength="50" value="<%=cstr(desfound)%>" required></td>
				<td align="right"> <%=cdbl(balfound)%> </td>
			</tr>
			<tr><center>
				<td colspan="6" align="center"><b><INPUT TYPE="SUBMIT" NAME="sbutton" VALUE="Modify"></b></td>
			</center></tr>
		</table>
		<input type="hidden" name="token" value="3"><p>
	</form>

	<%
	end if
	end sub
	sub pass3
	  response.write "<P><i>Pass 3 tokenvalue="+cstr(tokenvalue)

	  set cn=Server.CreateObject("ADODB.Connection")
	  cn.open "gl1536","gl1536","NFP32xcsX"
	  response.write "<P>Connection created OK"


	  SQLString="UPDATE glmaster SET "
	  SQLString=SQLString+ "acctdesc= "+ chr(39)+request.form("acctdesc")+chr(39)
	  SQLString=SQLString+ " WHERE major="+request.form("major")+" AND minor="+request.form("minor")+" AND sub1="+request.form("sub1")+" AND sub2="+request.form("sub2")+";"
	  response.write "<P>Ready for UPDATE with SQLString="+cstr(SQLString)

	  cn.execute SQLString,numa

	   if numa=1 then
	         response.write "<P>Modified OK"
	   else
	       if numa= 0 then
	           response.write "<P>Modify Failed. Number of records modified = "+cstr(numa)+ " record not found"
	       else
	           response.write "<P>Modify Occured WITH MORE THAN ONE RECORD. Number of records modified = "+cstr(numa)
	       end if
	   end if
	   cn.close
	   set cn=nothing
	   response.write "<P>Terminating normally"
	   response.write "</i> <p><a href='a42.asp'><button><<< Modify Another Account</button></a>"

	end sub

	sub passerror
	     response.write "<p>INVALID TOKEN VALUE. token="+cstr(tokernvalue)
	end sub

	'
	'*** top of main
	'
	tokenvalue=request.form("token")
	select case tokenvalue
	case ""
	   call pass1
	case "2"
	  call pass2
	case "3"
	  call pass3
	case else
	   call passerror
	end select

	%>
</body>
</html>